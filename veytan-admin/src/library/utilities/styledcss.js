import styled from 'styled-components';

export const MainWrapper = styled.div`
  // header
  .header-fixed {
    z-index: 1001;
    background-color: ${props => props.theme.header.backgroundColor} !important;
    border: solid 1px ${props => props.theme.header.borderColor};
    color: ${props => props.theme.header.textColor};
    .navbar-brand {
      color: ${props => props.theme.header.headerLogoColor};
    }
  }

  // sidemenu
  .custom-sidebar {
    background-color: ${props => props.theme.sidemenu.backgroundColor};
    a {
      color: ${props => props.theme.sidemenu.textColor} !important;
      &.active {
        .nav-item {
          color: ${props => props.theme.sidemenu.activeTextColor} !important;
          background: ${props => props.theme.sidemenu.activebackgroundColor};
        }
      }
      .nav-item {
        color: ${props => props.theme.sidemenu.textColor};
        &:hover {
          color: ${props => props.theme.sidemenu.hoverTextColor} !important;
          background: ${props => props.theme.sidemenu.hoverBackgroundColor};
        }
      }
    }
  }
  // main content
  .table {
    thead {
      th {
        background-color: ${props => props.theme.content.tableList.headerbackgroundColor};
        color: ${props => props.theme.content.tableList.headertextColor};
      }
    }
    td {
      color: ${props => props.theme.content.tableList.trTextColor};
    }
  }
`;
