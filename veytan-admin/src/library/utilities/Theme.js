export const theme = {
  header: {
    backgroundColor: '#fff',
    borderColor: '#ffffff',
    textColor: '#333',
    headerLogoColor: '#333',
  },
  sidemenu: {
    backgroundColor: '#263237',
    textColor: 'rgba(255, 255, 255, 0.7)',
    activebackgroundColor: '#3a4248',
    activeTextColor: '#63c2de',
    hoverBackgroundColor: '#63c2de',
    hoverTextColor: '#fff',
  },
  content: {
    tableList: {
      headerbackgroundColor: '#17a2b8',
      headertextColor: '#fff',
      trTextColor: '#333333',
    },
  },
};
