import { URLS } from 'library/common/constants/UrlConstants';
import Config from 'library/utilities/Config';

export const errorHandler = error => {
  if (error.response) {
    const loginURL = Config.BaseURL + URLS.login;
    if (error.response.status === 401) {
      if (error.response.config.url !== loginURL) {
        sessionStorage.clear();
        window.location.reload();
      }
    }
  }
};
