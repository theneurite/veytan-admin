export const URLS = {
  login: '/oauth/token',
  getLanguage: '/authenticated/translations/en_IN',
  logout: '/api/current-user/logout',
  profile: '/api/profiles/page',
  uploadFile: '/api/profiles/upload',
  uploadLeads: '/api/leads/upload',
  exportCsv: '/api/leads/export-csv',
  exportCompleted: '/api/profiles/completed/export-csv',
  load: '/api/profiles/add',
  sample: '/api/profiles/sample',
  release: '/api/profiles/release',
  completedProfileRow: id => `/api/profiles/${id}`,
};
