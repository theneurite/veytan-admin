import LocalizedStrings from 'react-localization';

import { eng } from './Languages/English';

const strings = new LocalizedStrings({
  eng,
});

export default strings;
