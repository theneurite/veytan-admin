import { Validators } from 'library/utilities/Validators';

export const formModel = [
  {
    label: 'Email',
    value: '',
    type: 'text',
    placeholder: 'Email',
    field: 'email',
    validators: [
      { check: Validators.required, message: 'Email is mandatory' },
      { check: Validators.email, message: 'Email is invalid' },
    ],
    required: true,
    styleClass: 'col-sm-12',
  },
];
