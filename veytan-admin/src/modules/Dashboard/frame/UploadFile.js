import React, { PureComponent } from 'react';
import axiosInstance from '../../../core/Axios';
import { URLS } from '../../../library/common/constants';
import { FileUploader, Loader } from '../../../library/common/components';
import ImageDisplay from './ImageDisplay';
import { cloneDeep } from 'lodash';
import ActionMessage from '../../../library/common/components/ActionMessage';

class UploadFile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      files: [],
      isLoading: false,
      contractTemplate: [],
      deletedContractTemplate: [],
      deletedFiles: [],
      messageBox: {
        display: false,
        type: '',
        message: '',
      },
    };
  }

  setActionMessage = (display = false, type = '', message = '') => {
    let setActionMessage = {
      display: display,
      type: type,
      message: message,
    };
    this.setState({ messageBox: setActionMessage });
  };

  UploadFIle = () => {
    const { files, contractTemplate } = this.state;
    if (Object.keys(files).length > 0 && Object.keys(contractTemplate).length > 0) {
      this.setActionMessage();
      this.setState({ isLoading: true });
      const body = new FormData();
      for (let i = 0; i < files.length; i++) {
        body.append('csvFile', files[i]);
      }

      for (let i = 0; i < contractTemplate.length; i++) {
        body.append('zipFile', contractTemplate[i]);
      }

      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      };

      axiosInstance
        .post(URLS.uploadFile, body, config)
        .then(({ status }) => {
          if (status === 200) {
            this.setState({ isLoading: false });
            this.setActionMessage(true, 'Success', 'Files successfully Uploaded');
          }
        })
        .catch(err => {
          this.setState({ isLoading: false });
          if (err.hasOwnProperty('response') && (err.response.status === 401 || err.response.status === 400)) {
            this.setActionMessage(true, 'Error', err.response.data.message);
          } else {
            this.setActionMessage(true, 'Error', "Something wen't wrong");
          }
        });
    } else {
      this.setActionMessage(true, 'Error', 'Please upload a valid .csv or .zip file');
    }
  };

  handleFileUpload = (files, index) => {
    const filesArray = files.map(file =>
      Object.assign(file, {
        mediaType: 'other',
      }),
    );
    switch (index) {
      case 1:
        this.setState({ files: filesArray });
        break;
      case 2:
        this.setState({ contractTemplate: filesArray });
        break;
      default:
        break;
    }
  };
  handleRemoveImage = (index, item) => () => {
    switch (item) {
      case 1:
        this.setState(state => {
          const files = state.files.filter((item, j) => index !== j);
          const deletedFile = state.files.filter((item, j) => index === j);
          const deletedList = [...this.state.deletedFiles];
          if (deletedFile.length > 0 && deletedFile[0].name !== 0 && deletedFile[0].name !== undefined) {
            deletedList.push(deletedFile[0].name);
            this.setState({ deletedFiles: deletedList });
          }
          return {
            files,
          };
        });
        break;
      case 2:
        this.setState(state => {
          const contractTemplate = state.contractTemplate.filter((item, j) => index !== j);
          const deletedContractTemplates = state.contractTemplate.filter((item, j) => index === j);
          const deletedList = [...this.state.deletedContractTemplate];
          if (
            deletedContractTemplates.length > 0 &&
            deletedContractTemplates[0].name !== 0 &&
            deletedContractTemplates[0].name !== undefined
          ) {
            deletedList.push(deletedContractTemplates[0].name);
            this.setState({ deletedContractTemplate: deletedList });
          }
          return {
            contractTemplate,
          };
        });
        break;
      default:
        break;
    }
  };
  handleMediaTypeChange = (file, index, value) => event => {
    const { files, contractTemplate } = this.state;
    const { value } = event.target;
    switch (value) {
      case 1:
        const filesArray = cloneDeep(files);
        filesArray[index].mediaType = value;
        this.setState({ files: filesArray });
        break;
      case 2:
        const contractTemplateArray = cloneDeep(contractTemplate);
        contractTemplateArray[index].mediaType = value;
        this.setState({ contractTemplate: contractTemplateArray });
        break;
      default:
        break;
    }
  };

  render() {
    const { files, contractTemplate, isLoading, messageBox } = this.state;
    return (
      <div>
        <div className="row section">
          <div className="col-sm-12 mb-1">
            <h3>File Upload</h3>
          </div>
          <div className="col-sm-12">
            {' '}
            {messageBox.display && (
              <ActionMessage message={messageBox.message} type={messageBox.type} styleClass={messageBox.styleClass} />
            )}{' '}
          </div>
          <div className="col-sm-12">{isLoading && <Loader />} </div>
          <div className="col-sm-6">
            <FileUploader
              multiple={false}
              accept=".csv"
              label="Upload csv file"
              onDrop={files => this.handleFileUpload(files, 1)}
            />
            <ImageDisplay
              files={files}
              onRemove={e => this.handleRemoveImage(e, 1)}
              onChangeMediaType={this.handleMediaTypeChange}
            />
          </div>
          <div className="col-sm-6">
            <FileUploader accept=".zip,.gz" label="Upload zip file" onDrop={files => this.handleFileUpload(files, 2)} />
            <ImageDisplay
              files={contractTemplate}
              onRemove={e => this.handleRemoveImage(e, 2)}
              onChangeMediaType={this.handleMediaTypeChange}
            />
          </div>

          <div className="col-sm-12">
            <div className="text-center mt-4">
              <button className="btn btn-primary mt-3" onClick={this.UploadFIle}>
                <i className="fa fa-upload mr-1" aria-hidden="true"></i> Upload File
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UploadFile;
