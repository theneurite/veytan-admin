import React from 'react';

import { Button } from 'library/common/components';

import './imageDisplayStyles.scss';

const ImageDisplay = ({ files, onRemove, onChangeMediaType }) => (
  <div className="image-display-list">
    {files.map((file, key) => (
      <div className="image-item" key={key}>
        <Button onClick={onRemove(key)} icon="fa-trash" styleClass="remove-button" />
        <div className="pdf" title={file.name}>
          <div className="pdf-file">
            <a target="_blank" rel="noopener noreferrer" href={file.preview}>
              {file.name}
            </a>
          </div>
          <div className="icon">
            <i className="fa fa-file" aria-hidden="true"></i>
          </div>
        </div>

        {/* <select className="form-control" onChange={onChangeMediaType(file, key)}>
          <option value="">Select Type</option>
          <option value="thumbnail">Thumbnail</option>
          <option value="main">Main</option>
          <option value="other">Other</option>
          <option value="none">None</option>
        </select> */}
      </div>
    ))}
  </div>
);

export default ImageDisplay;
