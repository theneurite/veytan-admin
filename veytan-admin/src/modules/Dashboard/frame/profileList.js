import React, { useRef, useState } from 'react';
import AudioPlayer, { RHAP_UI } from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';
import { ActionMessage, Button, Loader } from '../../../library/common/components';
import { URLS } from '../../../library/common/constants';
import axiosInstance from '../../../core/Axios';
import { connect } from 'react-redux';

const ProfileList = props => {
  const refInput = useRef();
  const [inputValue, setInput] = useState({
    name: '',
    pastExperience: '',
  });
  const [qcValue, setQcValue] = useState({
    qcVerifiedName: '',
    qcVerifiedPastExperience: '',
    correct: false,
  });
  const [profileList, getProfile] = useState();
  const [isLoading, setLoading] = useState(false);
  const [message, setMessage] = useState({
    display: false,
    type: '',
    message: '',
  });

  const setActionMessage = (display = false, type = '', message = '') => {
    setMessage({
      display,
      type,
      message,
    });
  };

  const nameAudio = (value, name) => {
    console.log(refInput.current);
    value = value.target.value;
    if (name === 'name') {
      if (props.role === 'ROLE_QC') {
        setQcValue({
          qcVerifiedName: value,
          qcVerifiedPastExperience: qcValue.qcVerifiedPastExperience,
          correct: false,
        });
      } else {
        setInput({
          name: value,
          pastExperience: inputValue.pastExperience,
        });
      }
    } else {
      if (props.role === 'ROLE_QC') {
        setQcValue({
          qcVerifiedName: qcValue.qcVerifiedName,
          qcVerifiedPastExperience: value,
          correct: false,
        });
      } else {
        setInput({
          name: inputValue.name,
          pastExperience: value,
        });
      }
    }
  };

  const downloadCsv = () => {
    setLoading(true);
    axiosInstance
      .get(URLS.exportCsv, {
        responseType: 'blob',
      })
      .then(({ status, data }) => {
        if (status === 200) {
          setLoading(false);
          const downloadUrl = window.URL.createObjectURL(new Blob([data]));
          const link = document.createElement('a');
          link.href = downloadUrl;
          link.setAttribute('download', 'leads.csv');
          document.body.appendChild(link);
          link.click();
          link.remove();
        }
      })
      .catch(err => {
        setLoading(false);
        if (err.hasOwnProperty('response') && (err.response.status === 401 || err.response.status === 400)) {
          setActionMessage(true, 'Error', err.response.data.message);
        } else {
          setActionMessage(true, 'Error', err);
        }
      });
  };

  const downloadCompleted = () => {
    setLoading(true);
    axiosInstance
      .get(URLS.exportCompleted, {
        responseType: 'blob',
      })
      .then(({ status, data }) => {
        if (status === 200) {
          setLoading(false);
          const downloadUrl = window.URL.createObjectURL(new Blob([data]));
          const link = document.createElement('a');
          link.href = downloadUrl;
          link.setAttribute('download', 'completed.csv');
          document.body.appendChild(link);
          link.click();
          link.remove();
        }
      })
      .catch(err => {
        setLoading(false);
        if (err.hasOwnProperty('response') && (err.response.status === 401 || err.response.status === 400)) {
          setActionMessage(true, 'Error', err.response.data.message);
        } else {
          setActionMessage(true, 'Error', err);
        }
      });
  };

  const load = () => {
    setLoading(true);
    axiosInstance
      .get(URLS.load)
      .then(({ status, data }) => {
        if (status === 200) {
          setLoading(false);
          setActionMessage(true, 'Success', data);
        }
      })
      .catch(err => {
        setLoading(false);
        if (err.hasOwnProperty('response') && (err.response.status === 401 || err.response.status === 400)) {
          setActionMessage(true, 'Error', err.response.data.message);
        } else {
          setActionMessage(true, 'Error', err);
        }
      });
  };

  const sample = () => {
    setLoading(true);
    axiosInstance
      .get(URLS.sample)
      .then(({ status, data }) => {
        if (status === 200) {
          setLoading(false);
          setActionMessage(true, 'Success', data);
        }
      })
      .catch(err => {
        setLoading(false);
        if (err.hasOwnProperty('response') && (err.response.status === 401 || err.response.status === 400)) {
          setActionMessage(true, 'Error', err.response.data.message);
        } else {
          setActionMessage(true, 'Error', err);
        }
      });
  };

  const release = () => {
    setLoading(true);
    axiosInstance
      .get(URLS.release)
      .then(({ status, data }) => {
        if (status === 200) {
          setLoading(false);
          setActionMessage(true, 'Success', data);
        }
      })
      .catch(err => {
        setLoading(false);
        if (err.hasOwnProperty('response') && (err.response.status === 401 || err.response.status === 400)) {
          setActionMessage(true, 'Error', err.response.data.message);
        } else {
          setActionMessage(true, 'Error', err);
        }
      });
  };

  const fetchRecord = () => {
    setLoading(true);
    axiosInstance
      .get(URLS.profile)
      .then(({ status, data }) => {
        if (status === 200) {
          getProfile(data);
          setLoading(false);
          if (data.length === 0) {
            setActionMessage(true, 'Success', 'No new profiles available. Please check back later.');
          } else {
            setActionMessage(true, 'Success', 'New Profiles retrieved for tagging !');
          }
          if (data) {
            addCheck(data);
          }
        }
      })
      .catch(err => {
        setLoading(false);
        if (err.hasOwnProperty('response') && (err.response.status === 401 || err.response.status === 400)) {
          setActionMessage(true, 'Error', err.response.data.message);
        } else {
          setActionMessage(true, 'Error', err);
        }
      });
  };

  const addCheck = data => {
    for (let i = 0; i < data.length; i++) {
      data[i].correct = false;
    }
    getProfile(data);
  };

  const submitData = id => {
    if (props.role === 'ROLE_QC') {
      const dataCheck = [...profileList];
      for (let i = 0; i < dataCheck.length; i++) {
        if (dataCheck[i].id === id) {
          qcValue.correct = dataCheck[i].correct;
        }
      }
    }

    const dataSend = props.role === 'ROLE_QC' ? qcValue : inputValue;
    setLoading(true);
    axiosInstance
      .put(URLS.completedProfileRow(id), dataSend)
      .then(({ status }) => {
        if (status === 200) {
          for (let i = 0; i < profileList.length; i++) {
            document.getElementById(`name${i}`).reset();
            document.getElementById(`exp${i}`).reset();
          }
          const filteredArray = profileList.filter(item => item.id !== id);
          getProfile(filteredArray);
          setInput({
            name: '',
            pastExperience: '',
          });
          setLoading(false);
          setActionMessage(true, 'Success', 'Tagged profile submitted!');
        }
      })
      .catch(err => {
        setLoading(false);
        if (err.hasOwnProperty('response') && (err.response.status === 401 || err.response.status === 400)) {
          setActionMessage(true, 'Error', err.response.data.message);
          const filteredArray = profileList.filter(item => item.id !== id);
          getProfile(filteredArray);
        } else {
          setActionMessage(true, 'Error', err);
        }
      });
  };

  const handleChange = (value, id) => {
    let data = [...profileList];
    for (let i = 0; i < data.length; i++) {
      if (data[i].id === id) {
        data[i].correct = !value;
      }
    }
    getProfile(data);
  };
  const uploadFiles = () => {
    props.propsValue.history.push('/upload-file');
  };

  const uploadLeads = () => {
    props.propsValue.history.push('/upload-leads');
  };

  return (
    <div>
      <div className="row">
        <div className="col-sm-12 mb-2">
          {(props.role === 'ROLE_USER' || props.role === 'ROLE_QC') && (
            <div>
              <span className="heading mr-4">Profiles</span>
              <Button styleClass="btn-primary mr-2" onClick={fetchRecord} value="Fetch Profiles" />
            </div>
          )}
          {(props.role === 'ROLE_ADMIN' || props.role === 'ROLE_IVR') && (
            <Button styleClass="btn-secondary mr-2" onClick={uploadFiles} value="Upload IVR Data" />
          )}
          {(props.role === 'ROLE_ADMIN' || props.role === 'ROLE_IVR') && (
            <Button styleClass="btn-secondary mr-2" value="Download Leads" onClick={() => downloadCsv()} />
          )}
          {props.role === 'ROLE_ADMIN' && (
            <Button styleClass="btn-secondary mr-2" onClick={uploadLeads} value="Upload Leads" />
          )}
          {props.role === 'ROLE_ADMIN' && (
            <Button
              styleClass="btn-secondary mr-2"
              value="Download Completed Profiles"
              onClick={() => downloadCompleted()}
            />
          )}
          {props.role === 'ROLE_ADMIN' && (
            <Button styleClass="btn-secondary mr-2" value="Load Profiles From S3" onClick={() => load()} />
          )}
          {props.role === 'ROLE_ADMIN' && (
            <Button styleClass="btn-secondary mr-2" value="Sample For QC" onClick={() => sample()} />
          )}
          {props.role === 'ROLE_ADMIN' && (
            <Button styleClass="btn-secondary mr-2" value="Release Profiles" onClick={() => release()} />
          )}
        </div>
        <div className="col-sm-6">
          {' '}
          {message.display && (
            <ActionMessage message={message.message} type={message.type} styleClass={message.styleClass} />
          )}
        </div>
        <div className="col-sm-12 p-3">
          {props.role === 'ROLE_USER' && (
            <h6>
              Thank you for helping us tag these profiles. Please click on the 'Fetch Profiles' button to retrieve
              profiles for tagging
            </h6>
          )}
          {(props.role === 'ROLE_ADMIN' || props.role === 'ROLE_IVR') && (
            <h6>Please use the buttons above to upload the IVR data and to download the leads for outbound calls.</h6>
          )}
          {props.role === 'ROLE_ADMIN' && (
            <h6>
              You can use the 'Upload Leads' button to load leads into the system. The file should be in a CSV format
              with headers 'Name' and 'Contact Number'.
            </h6>
          )}
          {props.role === 'ROLE_ADMIN' && (
            <h6>'Download Completed Profiles' - to download a list of completed profiles</h6>
          )}
          {props.role === 'ROLE_ADMIN' && (
            <h6>'Load Profiles from S3' - to manually load profiles from S3 (without waiting for the scheduler)</h6>
          )}
          {props.role === 'ROLE_ADMIN' && <h6>'Sample for QC' - Pick up a subset of completed records for QC</h6>}
          {props.role === 'ROLE_ADMIN' && (
            <h6>'Release Profiles' - Release already assigned profiles so that it can be picked up by others.</h6>
          )}
        </div>
        <div className="col-sm-1"> {isLoading && <Loader />}</div>
        <div className="col-sm-12">
          {profileList !== undefined && (
            <table className="table addontable">
              <thead>
                <tr>
                  {/*<th>#</th>*/}
                  <th>Name Audio</th>
                  <th>Name</th>
                  {/* {props.role === 'ROLE_USER' && <th>Name</th>} */}
                  {props.role === 'ROLE_QC' && <th>Corrected Name</th>}
                  <th>Experience Audio</th>
                  <th>Experience</th>
                  {/* {props.role === 'ROLE_USER' && <th>Experience</th>} */}
                  {props.role === 'ROLE_QC' && <th>Corrected Experience</th>}
                  {props.role === 'ROLE_QC' && <th>Tagging Correct</th>}
                  <th>Submit</th>
                </tr>
              </thead>
              <tbody>
                {profileList &&
                  profileList.map((item, i) => (
                    <tr>
                      {/*<td>{item.id}</td>*/}
                      <td>
                        <AudioPlayer
                          autoPlayAfterSrcChange={false}
                          src={item.nameAudioUrl}
                          showJumpControls={false}
                          customVolumeControls={[]}
                          customAdditionalControls={[]}
                          customControlsSection={[]}
                          customProgressBarSection={[RHAP_UI.MAIN_CONTROLS]}
                          layout="horizontal-reverse"
                        />
                      </td>
                      {props.role === 'ROLE_QC' && <td>{item.name}</td>}
                      <td>
                        <form id={`name${i}`}>
                          <input
                            id={`${i}`}
                            className="form-control"
                            type="text"
                            onChange={e => nameAudio(e, 'name')}
                          />
                        </form>
                      </td>
                      <td>
                        <AudioPlayer
                          autoPlayAfterSrcChange={false}
                          src={item.pastExpAudioUrl}
                          showJumpControls={false}
                          customVolumeControls={[]}
                          customAdditionalControls={[]}
                          customControlsSection={[]}
                          customProgressBarSection={[RHAP_UI.MAIN_CONTROLS]}
                          layout="horizontal-reverse"
                        />
                      </td>
                      {props.role === 'ROLE_QC' && <td>{item.pastExperience}</td>}
                      <td>
                        <form id={`exp${i}`}>
                          <input className="form-control" type="text" onChange={e => nameAudio(e, 'exp')} />
                        </form>
                      </td>
                      {props.role === 'ROLE_QC' && (
                        <td>
                          <i
                            onClick={() => handleChange(item.correct, item.id)}
                            className={`checkIcon fa fa-${item.correct ? 'check-square' : 'square-o'} `}
                            aria-hidden="true"
                          />
                        </td>
                      )}
                      <td>
                        <Button value="Submit" onClick={() => submitData(item.id)} />
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          )}
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = ({ authReducer }) => {
  return {
    role: authReducer.token.role,
  };
};

export default connect(mapStateToProps, {})(ProfileList);
