import React, { PureComponent } from 'react';
import ProfileList from './frame/profileList';
import { connect } from 'react-redux';

class Dashboard extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <ProfileList role={this.props.role} propsValue={this.props} />
      </div>
    );
  }
}

const mapStateToProps = authReducer => {
  return {
    role: authReducer.authReducer.token.role,
  };
};
export default connect(mapStateToProps, {})(Dashboard);
