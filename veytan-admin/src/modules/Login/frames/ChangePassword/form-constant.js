import { Validators } from 'library/utilities/Validators';
import { strings } from '../../../../library/common/components';

export const changePasswordFormModel = [
  {
    label: `${strings.changePassword_text_oldPassword}`,
    value: '',
    type: 'password',
    placeholder: 'Old Password',
    field: 'oldPassword',
    validators: [{ check: Validators.required, message: 'Old password is mandatory' }],
    required: true,
    styleClass: 'col-sm-12',
  },
  {
    label: `${strings.changePassword_text_newPassword}`,
    value: '',
    type: 'password',
    placeholder: 'New Password',
    field: 'newPassword',
    validators: [{ check: Validators.required, message: 'New password is mandatory' }],
    required: true,
    styleClass: 'col-sm-12',
  },
  {
    label: `${strings.changePassword_text_confirmPassword}`,
    value: '',
    type: 'password',
    placeholder: 'Confirm Password',
    field: 'confirmPassword',
    validators: [{ check: Validators.required, message: 'Confirm password is mandatory' }],
    required: true,
    styleClass: 'col-sm-12',
  },
];
