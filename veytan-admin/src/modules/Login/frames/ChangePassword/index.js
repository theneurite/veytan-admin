import React, { useRef, useState } from 'react';
import * as $ from 'jquery';
import { withRouter } from 'react-router-dom';
import axiosInstance from 'core/Axios';
import { URLS } from 'library/common/constants/UrlConstants';
import { fetchErrorFields, removeErrorFieldsFromValues } from 'library/utilities';
import { ActionMessage, Button, Form, Loader } from 'library/common/components';
import { connect } from 'react-redux';
import { changePasswordFormModel } from './form-constant';
import { strings } from '../../../../library/common/components';

const AddPayment = ({ isLoggedIn, history }) => {
  // const { isLoggedIn } = this.props;
  let formRef = useRef();
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState({
    display: false,
    type: '',
    message: '',
  });
  const handleSave = e => {
    e.preventDefault();
    if (formRef.getFormData().isFormValid) {
      setLoading(true);
      const data = removeErrorFieldsFromValues(formRef.getFormData().formData);
      if (data.newPassword === data.confirmPassword) {
        axiosInstance
          .put(URLS.changePassword, data)
          .then(({ status }) => {
            if (status === 200) {
              setLoading(false);
              setActionMessage(true, 'Success', 'Password change successfully');
              setTimeout(() => {
                setActionMessage(false, '', '');
                closeModal();
                if (isLoggedIn) {
                  history.push('/');
                }
              }, 1000);
              // closeModal();
              // if (isLoggedIn) {
              //   history.push('/');
              // }
            }
          })
          .catch(err => {
            setLoading(false);
            if (err.hasOwnProperty('response') && err.response.status === 404) {
              setActionMessage(true, 'Error', err.response.data.message);
            } else if (err.hasOwnProperty('response') && err.response.status === 400) {
              setActionMessage(true, 'Error', err.response.data.message);
            }
          });
      } else {
        setActionMessage(true, 'Error', 'password mismatch');
        setLoading(false);
      }
    } else {
      setActionMessage(
        true,
        'Error',
        `Please fill details for ${fetchErrorFields(formRef.getFormData().formData, changePasswordFormModel).join(
          ', ',
        )} `,
      );
    }
  };

  const setActionMessage = (display = false, type = '', message = '') => {
    setMessage({
      display,
      type,
      message,
    });
  };

  const closeModal = () => {
    setMessage(false);
    setLoading(false);
    if (formRef) {
      formRef.resetForm();
    }
    $('#change-password').modal('hide');
  };

  return (
    <div
      className="modal fade"
      id="change-password"
      tabIndex="-1"
      role="dialog"
      data-backdrop="static"
      aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content rounded-0">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalCenterTitle">
              {strings.changePassword_text_changePassword}
            </h5>
            {loading && <Loader />}
            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={closeModal}>
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            {message.display && (
              <ActionMessage message={message.message} type={message.type} styleClass={message.styleClass} />
            )}
            <Form model={changePasswordFormModel} ref={el => (formRef = el)} />
            <Button
              value={strings.common_button_back}
              icon="fa-times-circle"
              styleClass="btn-danger mr-3"
              onClick={closeModal}
            />
            <Button value="Submit" icon="fa-save" onClick={handleSave} />
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = ({ authReducer }) => {
  return {
    isLoggedIn: authReducer.isLoggedIn,
  };
};

export default withRouter(connect(mapStateToProps, {})(AddPayment));
