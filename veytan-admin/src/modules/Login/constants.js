import { Validators } from '../../library/utilities/Validators';
import { strings } from '../../library/common/components';

export const loginBg = require('../../resources/images/logo2.jpg');

export const loginFormModel = [
  {
    label: '',
    value: '',
    type: 'text',
    placeholder: strings.emailText,
    field: 'username',
    validators: [
      { check: Validators.required, message: 'Email is mandatory' },
      { check: Validators.email, message: 'Email is invalid' },
    ],
    required: true,
    styleClass: 'col-sm-12',
  },
  {
    label: '',
    value: '',
    type: 'password',
    placeholder: strings.PasswordText,
    field: 'password',
    validators: [{ check: Validators.required, message: 'password is mandatory' }],
    required: true,
    styleClass: 'col-sm-12',
  },
];
