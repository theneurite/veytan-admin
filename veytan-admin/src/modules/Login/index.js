import React, { Component, createRef } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as $ from 'jquery';
import { getAuth, setAuthentication } from 'library/common/actions';
import { Button, Form } from 'library/common/components';
import { URLS } from 'library/common/constants';
import axiosInstance from 'core/Axios';
import ChangePassword from 'modules/Login/frames/ChangePassword';
import { loginBg, loginFormModel } from './constants';
import './loginStyles.scss';
import Config from 'library/utilities/Config';
import { strings } from '../../library/common/components';

export class Login extends Component {
  formRef = createRef();

  state = {
    isLoading: false,
    loginError: null,
  };

  componentDidMount() {
    document.title = 'Login';
    this.props.getAuth();
  }

  handleLogin = async e => {
    e.preventDefault();
    const values = this.formRef.getFormData();
    const { formData, isFormValid } = values;
    // const { REACT_APP_CLIENT, REACT_APP_SECRET } = process.env;

    if (isFormValid) {
      this.setState({ isLoading: true });

      const body = new URLSearchParams();
      body.append('username', formData.username);
      body.append('password', formData.password);
      body.append('scope', 'webclient');
      body.append('grant_type', 'password');

      const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        auth: {
          username: Config.REACT_APP_CLIENT,
          password: Config.REACT_APP_SECRET,
        },
      };

      axiosInstance
        .post(URLS.login, body, config)
        .then(({ status, data }) => {
          if (status === 200) {
            this.setState({ isLoading: false });
            this.props.setAuthentication(data);
            this.props.history.push('/dashboard');
          }
        })

        .catch(err => {
          if (err.hasOwnProperty('response') && (err.response.status === 401 || err.response.status === 400)) {
            this.setState({ isLoading: false, loginError: 'Login failed. Please check credentials again.' });
          } else {
            this.setState({ isLoading: false, loginError: 'Something went wrong. Please try again.' });
          }
        });
    } else {
      this.setState({ isLoading: false, loginError: strings.loginFormError });
    }
  };

  openChangePasswordModal = () => {
    $(`#change-password`).modal('show');
  };

  render() {
    // const { isLoggedIn } = this.props;
    const { isLoading, loginError } = this.state;
    // if (isLoggedIn) {
    //   return <Redirect to="/" />;
    // }
    return (
      <div className="vh-100 login bg-white d-flex justify-content-center align-items-center">
        <div className="col-sm-4 login-background vh-100 p-0">
          <img src={loginBg} alt="background" />
        </div>
        <div className="col-sm-8 login-form vh-100 d-flex justify-content-center align-items-center">
          <div className="col-sm-5">
            <h4 className="pb-4 welcomeText"> {strings.loginWelcomeText} </h4>{' '}
            {!!loginError && <p className="text-danger"> {loginError} </p>}{' '}
            <form onSubmit={this.handleLogin}>
              <Form
                ref={el => {
                  this.formRef = el;
                }}
                model={loginFormModel}
              />{' '}
              <Button
                type="submit"
                loading={isLoading}
                onClick={this.handleLogin}
                styleClass="btn-success btn-block"
                value={strings.loginText}
              />
            </form>
            <ChangePassword />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ authReducer }) => {
  return {
    isLoggedIn: authReducer.isLoggedIn,
    permission: authReducer.token,
  };
};

export default withRouter(connect(mapStateToProps, { getAuth, setAuthentication })(Login));
