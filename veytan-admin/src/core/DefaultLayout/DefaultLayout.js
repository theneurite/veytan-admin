import React, { Component, lazy, Suspense } from 'react';
import { connect } from 'react-redux';
import { logoutUser } from 'library/common/actions';
import { Loader } from 'library/common/components';

const DefaultHeader = lazy(() => import('./DefaultHeader'));

export class DefaultLayout extends Component {
  handleLogout = () => {
    this.props.logoutUser();
  };

  render() {
    const { user } = this.props;
    return (
      <div>
        <Suspense fallback={<Loader />}>
          <DefaultHeader onLogout={this.handleLogout} user={user} />
        </Suspense>

        <div className="container-fluid">
          <div className="row" style={{ marginTop: '56px' }}>
            <div className="col-sm-12 main-container">
              <div className="card shadow-sm mt-3 mb-3">
                <div className="card-body">
                  <Suspense fallback={<Loader />}>{this.props.children}</Suspense>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ authReducer }) => {
  return {
    isLoggedIn: authReducer.isLoggedIn,
    user: authReducer.token,
  };
};

export default connect(mapStateToProps, { logoutUser })(DefaultLayout);
