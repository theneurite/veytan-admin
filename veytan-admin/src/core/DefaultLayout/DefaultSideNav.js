import React, { PureComponent } from 'react';
import SideMenu from 'react-sidemenu';
import { Scrollbars } from 'react-custom-scrollbars';
import 'react-sidemenu/dist/side-menu.css';
import { strings } from '../../library/common/components';

export default class DefaultSideNav extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { navBar: [] };
  }

  componentDidUpdate(prevProps) {
    const { translation } = this.props;
    if (prevProps.translation !== translation) {
      this.setSideMenu();
    }
  }

  setSideMenu = () => {
    setTimeout(() => {
      this.setState(
        {
          navBar: [],
        },
        () => {
          const { navBar } = this.state;
          if (navBar) {
            const array = [...navBar];
            for (let i = array.length - 1; i >= 0; i--) {
              if (array[i] === '') {
                array.splice(i, 1);
              } else {
                if (array[i].children && array[i].children.length > 0) {
                  for (let j = array[i].children.length - 1; j >= 0; j--) {
                    if (array[i].children[j] === '') {
                      array[i].children.splice(j, 1);
                    }
                  }
                }
              }
            }
            this.setState({ navBar: array });
          }
        },
      );
    }, 100);
  };

  render() {
    const { navBar } = this.state;
    const { isToggle } = this.props;
    return (
      <div className={`sidebar custom-sidebar ${!isToggle && 'sidebar-toggle'}`}>
        <Scrollbars
          autoHeight
          autoHeightMin="91vh"
          autoHide
          autoHideTimeout={1000}
          autoHideDuration={200}
          autoHeightMax="91vh">
          <SideMenu shouldTriggerClickOnParents={false} items={navBar} activeItem={this.props.locationPath} />
        </Scrollbars>
      </div>
    );
  }
}
