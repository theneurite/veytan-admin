import React, { useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import './sidebarStyles.scss';
import AddPayment from '../../modules/Login/frames/ChangePassword';

const DefaultHeader = ({ onLogout, user, toggleSidebar }) => {
  const [toogle, setToggle] = useState(false);
  const isToggleSidebar = () => {
    setToggle(!toogle);
    toggleSidebar(toogle);
  };
  return (
    <div>
      <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-white border-bottom header-fixed">
        <NavLink className="navbar-brand" to="/">
          VEYTAN
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item border-right pr-3">
              <p className="mb-0 menu-item changePassword">
                <Link to="/userProfile">
                  {' '}
                  <i className="fa fa-user-circle mr-1" />
                  <span>{user && user.username}</span>
                </Link>
              </p>
            </li>
            <li className="nav-item pl-3">
              <h4 className="mb-0 cursor-pointer logoutButton" onClick={onLogout}>
                <i className="fa fa-power-off" />
              </h4>
            </li>
          </ul>
        </div>
      </nav>
      <AddPayment />
    </div>
  );
};

export default DefaultHeader;
