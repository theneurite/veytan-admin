import { connect } from 'react-redux';
import React, { lazy } from 'react';
import { Redirect, Route, Switch, useLocation, withRouter } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import PrivateRoute from './PrivateRoute';
import { MainWrapper } from '../../library/utilities/styledcss';
import { fetchFromStorage } from '../../library/utilities';
import { theme } from '../../library/utilities/Theme';
import UploadFile from '../../modules/Dashboard/frame/UploadFile';
import UploadLeads from '../../modules/Dashboard/frame/UploadLeads';

const Dashboard = lazy(() => import('modules/Dashboard'));
const Login = lazy(() => import('modules/Login'));

const Routes = props => {
  const newTheme = fetchFromStorage('theme');
  const { isLoggedIn } = props;
  const location = useLocation();
  const { from } = location.state || { from: { pathname: '/dashboard' } };
  return (
    <div>
      <ThemeProvider theme={newTheme || theme}>
        <MainWrapper>
          <Switch>
            <Route
              exact
              path="/"
              render={() => {
                if (isLoggedIn && from) {
                  return <Redirect to={from} />;
                } else {
                  return <Redirect to="/login" />;
                }
              }}
            />

            <Route exact path="/login" component={Login} />
            <PrivateRoute exact isLoggedIn={isLoggedIn} path="/dashboard" component={Dashboard} />
            <PrivateRoute exact isLoggedIn={isLoggedIn} path="/upload-file" component={UploadFile} />
            <PrivateRoute exact isLoggedIn={isLoggedIn} path="/upload-leads" component={UploadLeads} />
          </Switch>
        </MainWrapper>
      </ThemeProvider>
    </div>
  );
};

const mapStateToProps = ({ authReducer }) => {
  return {
    isLoggedIn: authReducer.isLoggedIn,
    translation: authReducer.translation,
  };
};

export default withRouter(connect(mapStateToProps, {})(Routes));
